public class Student{
    public String name;
    public String favoriteCourse;
    public String favoriteCollege;
    public int RScore;
    
    public void greet(){
        System.out.println("Hi! Nice to meet you! My name is " + name);
        System.out.println("My favorite course is " + favoriteCourse + " and I would love to be accepted in " + favoriteCollege);
    }
    public boolean applyCollege(){
        if (RScore > 30){
            return true;
        }else {
            return false;
        }
    }
    
}