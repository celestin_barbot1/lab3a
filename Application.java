import java.util.Scanner;
public class Application{
    public static void main(String[] args) {
        Student student1 = new Student();       
        Student student2 = new Student();     
        Scanner reader = new Scanner(System.in);
        student1.favoriteCourse = "Calculus";
        student1.favoriteCollege = "UQAM";
        student1.name = "Jon Smith";
        student1.RScore = 34;
        
        student2.favoriteCourse = "Phys ed";
        student2.favoriteCollege = "Mcgill";
        student2.name = "Smith Jon";
        student2.RScore = 3;

        Student[] section4 = new Student[3];
        section4[0] = student1;
        section4[1] = student2;
        section4[2] = new Student();

        System.out.println();



        // System.out.println("Which student do you want to talk to?\n Student 1 or 2?");
        // int choice = reader.nextInt();
        // if (choice == 1){
        //     student1.greet();
        //     student1.applyCollege();
        // } else if (choice == 2){
        //     student2.greet();
        //     student2.applyCollege();
        // } else {
        //     System.out.println("Wrong number, this student doesn't exist :(");
        // }


    }
}